<?php

namespace Insolutions\Scanner\Models;

use Illuminate\Database\Eloquent\Model;

class CodeType extends Model
{
    protected $table = 'enm_scan_code_type';

    public static function findByName($code_type) {
    	return self::where(['code_type' => $code_type])->first();
    }
}