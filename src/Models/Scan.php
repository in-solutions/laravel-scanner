<?php

namespace Insolutions\Scanner\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Insolutions\Scanner\Models\CodeType;

class Scan extends Model
{
	use SoftDeletes;

    protected $table = 't_scan';

    protected $fillable = ['title', 'description'];

    protected $hidden = ['deleted_at','created_at','code_type_id'];

    protected $with = ['codeType'];

    public static function createForCode($code) {
    	$scan = new self;
    	$scan->code = $code;
    	return $scan;
    }

    public static function findByCode($code) {
    	return self::where(['code' => $code])->first();
    }

    public function codeType() {
    	return $this->belongsTo(CodeType::class);
    }

}