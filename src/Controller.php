<?php

namespace Insolutions\Scanner;
 
use Illuminate\Http\Request;

use Insolutions\Scanner\Models\Scan;

class Controller extends \App\Http\Controllers\Controller
{

	public function list(Request $r) {
		return response()->json(
			Scan::paginate($r->perPage ?: 50)
		);
	}

	public function load(Request $r, $code) {
		$scan = Scan::findBycode($code);
		if ($scan) {
			return response()->json($scan);
		} else {
			abort(404, "No entry for given code");
		}
	}

	public function save(Request $r, $code) {
		$scan = Scan::findBycode($code);
		if (!$scan) {
			$scan = new Scan;
			$scan->code = $code;
		}

		$scan->fill($r->all());
		$scan->save();

		return response()->json($scan, 200);
	}

}