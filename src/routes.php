<?php

use Insolutions\Scanner\Controller;

Route::group(['prefix' => 'scanner'], function () {
	
	Route::get('/scan', Controller::class . '@list');
	Route::get('/scan/{code}', Controller::class . '@load');
	Route::post('/scan/{code}', Controller::class . '@save');

});