SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `enm_scan_code_type` (
`id` int(10) unsigned NOT NULL,
  `code_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_scan` (
`id` bigint(20) unsigned NOT NULL,
  `code_type_id` int(11) unsigned DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `enm_scan_code_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code_type` (`code_type`);

ALTER TABLE `t_scan`
 ADD PRIMARY KEY (`id`), ADD KEY `code_type` (`code_type_id`);


ALTER TABLE `enm_scan_code_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_scan`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;